package HW6;

import java.util.Arrays;

public final class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name, surname, year, iq, pet, schedule);
    }

    @Override
    public void greetPet() {
        if (super.getFamily() != null && super.getFamily().getPet() != null) {
            System.out.println("Hi, " + this.getFamily().getPet().getNickname() + "! - woman said");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname. - woman said");
        }
    }

    public void makeup(){
        System.out.println("I've finished make up. I'm beautiful!");
    }

    @Override
    public String toString() {
        return "Woman{" +
                "name='" + super.getName() + '\'' +
                ", surname='" + super.getSurname() + '\'' +
                ", year=" + super.getYear() +
                ", iq=" + super.getIq() +
                ", schedule=" + Arrays.deepToString(super.getSchedule()) +
                '}';
    }


}
