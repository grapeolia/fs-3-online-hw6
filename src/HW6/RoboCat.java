package HW6;

public class RoboCat extends Pet{
    public RoboCat() {
        this.species = species;
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies(){
        return this.species;
    }

    public void setSpecies(){
        this.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
