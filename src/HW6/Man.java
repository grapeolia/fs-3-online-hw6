package HW6;

import java.util.Arrays;

public final class Man extends Human{
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Pet pet, String[][] schedule) {
        super(name, surname, year, iq, pet, schedule);
    }

    @Override
    public void greetPet() {
        if (super.getFamily() != null && super.getFamily().getPet() != null) {
            System.out.println("Hi, " + this.getFamily().getPet().getNickname() + "! - man said");
        } else {
            System.out.println("I don't have any pet. Or my pet doesn't have any nickname. - man said");
        }
    }

    public void repairCar(){
        System.out.println("Finally! I've finished repairing my car.");
    }


    @Override
    public String toString() {
        return "Man{" +
                "name='" + super.getName() + '\'' +
                ", surname='" + super.getSurname() + '\'' +
                ", year=" + super.getYear() +
                ", iq=" + super.getIq() +
                ", schedule=" + Arrays.deepToString(super.getSchedule()) +
                '}';
    }

}
