package HW6;

public class Dog extends Pet implements Pest{
    Species species = Species.DOG;
    public Dog() {
    }

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public Species getSpecies(){
        return this.species;
    }

    public void setSpecies(){
        this.species = Species.DOG;
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks");
    }

    @Override
    public void respond() {
        System.out.println("Hi, owner! I'm "+this.nickname+". I miss you.");
    }
}
