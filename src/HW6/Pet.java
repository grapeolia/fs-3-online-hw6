package HW6;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    public Species species;
    public String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
        this.species = Species.UNKNOWN;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        setAge(age);
        setTrickLevel(trickLevel);
        this.habits = habits;
        this.species = Species.UNKNOWN;

    }

    abstract Species getSpecies();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age >= 0 && age <= 30){
            this.age = age;
        } else {
            System.out.println("Age must be number 0...30. Age 0 has been set by default.");
            this.age = 0;
        }
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if(trickLevel>=0 && trickLevel <=100){
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Trick Level must be number 0...100. Trick Level 0 has been set by default.");
            this.trickLevel = 0;
        }
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet pet)) return false;
        return age == pet.age && trickLevel == pet.trickLevel && species.equals(pet.species) && nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel, Arrays.hashCode(habits));
    }

    public void eat (){
        System.out.println("I'm eating");
    }

    public abstract void respond ();

    //`dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}`, где `dog` - вид животного;

    @Override
    public String toString() {
        return this.getSpecies() +"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }
}
